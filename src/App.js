import React, {Component} from 'react';
import './App.css';
import Header from "./component/header";
import Form from "./component/formUI/form";
import Task from "./component/message/Task";

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            value : "",
            list : [
                {id: 0, value: "Ich bin Muradil"},
                {id: 1, value: "Bist du programmer?"},
                {id: 2, value: "Ich weiss react.js"}
            ]
        };
    }

    change = event =>{
      this.setState({
          value : event.target.value
      });
    };

    submit = () =>{
      if (this.state.value === ""){
          return false
      }else {
          const list = [...this.state.list, {id: this.state.list[this.state.list.length - 1] + 1, value: this.state.value}];
          this.setState({
              value : '',
              list: list
          });
      }
    };

    remove = id =>{
      let list  = this.state.list;
      let index = list.findIndex(task=> task.id === id);
      list.splice(index, 1);

      this.setState({
          list: list,
      })

    };


    render() {
        let list  = this.state.list.map((task) => {
            return(
                <Task key={task.id}
                    remove={() => this.remove(task.id)}
                    value={task.value}
                />
            )
        });
        return (
        <div className="App">
            <Header/>
            <Form
                change={event => this.change(event)}
                submit={() => this.submit()}
            />
            {list}
        </div>
    );
    }


}

export default App;
