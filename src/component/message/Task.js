import React from 'react';
import '../../App.css'

const Task = (props) => {
    return (
        <p className="messageUI">{props.value}
            <button className="close" onClick={props.remove}>Delete</button>
        </p>
    );
};

export default Task;