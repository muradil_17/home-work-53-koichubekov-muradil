import React from 'react';

const Form = (props) => {
    return (
        <div className="formUI">
            <form action="#">
                <input type="text"
                        className="message"
                        placeholder="Введите список дел"
                        onChange={props.change}
                />
                <button className='button' onClick={props.submit}>add</button>
            </form>
        </div>
    );
};

export default Form;